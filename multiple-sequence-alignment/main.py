from sys import argv

if len(argv) < 2:
    print("You need to use: python main.py <sequences_filename.fasta> <scores_filename.txt>")
    exit()

from Bio import SeqIO
import numpy as np
from FengDoolittle import FengDoolittle


with open("pam250.txt", 'r') as f:
    alphabet = f.readline() 
    while(alphabet[-1] in ['\n', '\r']): 
        alphabet = alphabet[:-1] 
    l = len(alphabet) 
    if l == 0:
        print("alphabet must be not empty")
        exit()

    f.readline()
    d = int(f.readline()[:-1])
    f.readline() 
    simMatrix = np.ndarray((l, l)) 
    line = f.readline() 
    for i in range(l): 
        row = np.array(line.split()).astype(np.int32) 
        simMatrix[i] = row
        line = f.readline() 
    
seqs = []
for s in SeqIO.parse(argv[1], "fasta"):
    for a in s.seq:
        if a not in alphabet:
            print("sequnces must contain only letters from alphabet")
            exit()
    seqs.append(s.seq)
if len(seqs) < 2: 
    print("Fasta file must contains more then two sequences")
    exit()
 
alphEnum = dict([(alphabet[i], i) for i in range(len(alphabet))])  

fd = FengDoolittle(seqs, simMatrix, d, alphEnum)
res = fd.computeMultipleAlignment()
print(*res, sep="\n")