import numpy as np


class Upgma():
    def __init__(self, distance_dictionary, node_count):
        self.distance_dictionary = distance_dictionary
        self.mapping = {}
        self.node_count = node_count
        self.number_of_nodes = node_count
        

    def compute_clustering(self):
        computation_is_done = False
        j = 0
        while not computation_is_done:
            j += 1
            minimum_cluster = self.compute_minimal_distance()
            nodes = minimum_cluster[0].split(" ")
            if len(nodes) > 1:
                self.mapping[minimum_cluster[0]] = self.node_count
                
                if minimum_cluster[0] in self.distance_dictionary:
                    del self.distance_dictionary[minimum_cluster[0]]

                for i in range(0, self.node_count + 1):
                    key_value_0 = nodes[0] + " " + str(i)
                    key_value_1 = nodes[1] + " " + str(i)
                    key_value = self.key_in_dictionary(key_value_0, key_value_1)
                    if key_value[0] != "":
                        key_for_new_cluster_distance = str(i) + " " + str(self.node_count)
                        self.distance_dictionary[key_for_new_cluster_distance] = self.compute_new_distance(
                            self.distance_dictionary[key_value[0]], self.distance_dictionary[key_value[1]])
                
                        
                        del self.distance_dictionary[key_value[0]]
                        del self.distance_dictionary[key_value[1]]
                self.node_count += 1
            else:
                computation_is_done = True

    def key_in_dictionary(self, key_value_0, key_value_1):
        """Returns True if the given keys are in the distance dictionary, False otherwise.
            key_value_0: The first key value.
            key_value_1: The second key value."""
        for i in range(0, 4):
            if key_value_0 in self.distance_dictionary and key_value_1 in self.distance_dictionary:
                return [key_value_0, key_value_1]
            elif key_value_0[::-1] in self.distance_dictionary and key_value_1 in self.distance_dictionary:
                return [key_value_0[::-1], key_value_1]
            elif key_value_0 in self.distance_dictionary and key_value_1[::-1] in self.distance_dictionary:
                return [key_value_0, key_value_1[::-1]]
            elif key_value_0[::-1] in self.distance_dictionary and key_value_1[::-1] in self.distance_dictionary:
                return [key_value_0[::-1], key_value_1[::-1]]
            else:
                return ["", ""]


    def compute_minimal_distance(self):
        minimum = ["", np.inf]
        for i in self.distance_dictionary:
            if minimum[1] > self.distance_dictionary[i]:
                minimum[0] = i
                minimum[1] = self.distance_dictionary[i]
        return minimum

    def compute_new_distance(self, distance_a_x, distance_b_x):
        return (distance_a_x + distance_b_x) / 2
        
    

    
    def get_newick_tree(self, with_edge_weights=False):
        newick_dictionary = dict([[v, k] for k, v in self.mapping.items()])
        if with_edge_weights:
            for i in newick_dictionary:
                if i in self.edge_weight:
                    nodesWithWeights = newick_dictionary[i].split(" ")
                    nodesWithWeights[0] = nodesWithWeights[0].strip(" ")
                    nodesWithWeights[0] += ":" + str(self.edge_weight[i][1])
                    nodesWithWeights[1] = nodesWithWeights[1].strip(" ")
                    nodesWithWeights[1] += ":" + str(self.edge_weight[i][0])
                    newick_dictionary[i] = nodesWithWeights[0] + " " + nodesWithWeights[1]
            self.mapping = dict([[v, k] for k, v in newick_dictionary.items()])
        for i in self.mapping:
            index = -1
            leading_sequence = True
            for j in newick_dictionary:
                string_to_find = " " + str(self.mapping[i]) + ""
                if newick_dictionary[j].find(string_to_find) != -1:
                    index = j
                    leading_sequence = False
                    break
                string_to_find = str(self.mapping[i]) + " "
                if newick_dictionary[j].find(string_to_find) != -1:
                    index = j
                    leading_sequence = True
                    break
                if with_edge_weights:
                    string_to_find = str(self.mapping[i]) + ":"
                else:
                    string_to_find = str(self.mapping[i]) + ","
                if newick_dictionary[j].find(string_to_find) != -1:
                    index = j
                    leading_sequence = True
                    break
                string_to_find = "," + str(self.mapping[i])
                if newick_dictionary[j].find(string_to_find) != -1:
                    index = j
                    leading_sequence = False
                    break

            if index != -1:
                if leading_sequence:
                    stringToReplace = "(" + newick_dictionary[int(string_to_find.strip().strip(",").strip(":"))].replace(" ",
                                                                                                              ",") + "):"
                else:
                    stringToReplace = ",(" + newick_dictionary[int(string_to_find.strip().strip(",").strip(":"))].replace(" ",
                                                                                                               ",") + ")"
                newick_dictionary[index] = newick_dictionary[index].replace(string_to_find, stringToReplace).replace(
                    ",,", ",")
                del newick_dictionary[int(string_to_find.strip().strip(",").strip(":"))]

        for i in newick_dictionary:
            return "(" + newick_dictionary[i] + ")"

