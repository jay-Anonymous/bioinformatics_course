import numpy as np
from Upgma import Upgma


class FengDoolittle():
    def __init__(self, sequences, simMatrix, gapPenalty, alphEnum):
        self.sequences = sequences
        self.alignments = {}
        self.simMatrix = simMatrix
        self.gapPenalty = gapPenalty
        self.alphEnum = alphEnum
        self.distanceDictionary = {}
        self.newickTree = ""
        self.orderToAlign = []


    def NeedlemanWunsch(self, A, B): 
        n, m = len(A), len(B) 
        mat = [] 
        for i in range(n+1): 
            mat.append([0]*(m+1)) 
        for j in range(m+1): 
            mat[0][j] = self.gapPenalty*j 
        for i in range(n+1): 
            mat[i][0] = self.gapPenalty*i 
        for i in range(1, n+1): 
            for j in range(1, m+1): 
                mat[i][j] = max(mat[i-1][j-1] + self.simMatrix[self.alphEnum[A[i-1]]][self.alphEnum[B[j-1]]], mat[i][j-1] + self.gapPenalty, mat[i-1][j] + self.gapPenalty) 
    
        alignmentA = "" 
        alignmentB = "" 
        i, j = n, m 
        while i and j: 
            score, scoreDiag, scoreUp, scoreLeft = mat[i][j], mat[i-1][j-1], mat[i-1][j], mat[i][j-1] 
            if score == scoreDiag + self.simMatrix[self.alphEnum[A[i-1]]][self.alphEnum[B[j-1]]]: 
                alignmentA = A[i-1] + alignmentA 
                alignmentB = B[j-1] + alignmentB 
                i -= 1 
                j -= 1 
            elif score == scoreUp + self.gapPenalty: 
                alignmentA = A[i-1] + alignmentA 
                alignmentB = '-' + alignmentB 
                i -= 1 
            elif score == scoreLeft + self.gapPenalty: 
                alignmentA = '-' + alignmentA 
                alignmentB = B[j-1] + alignmentB 
                j -= 1 
        while i: 
            alignmentA = A[i-1] + alignmentA 
            alignmentB = '-' + alignmentB 
            i -= 1 
        while j: 
            alignmentA = '-' + alignmentA 
            alignmentB = B[j-1] + alignmentB 
            j -= 1 
        return [alignmentA, alignmentB, mat[n][m]] 
        

    def compute_D(self): 
        count = len(self.sequences)
        D = {} 
        S_ii = np.ndarray(count) 
        for i in range(count):
            _, _, S_ii[i] = self.NeedlemanWunsch(self.sequences[i], self.sequences[i]) 
        for i in range(count): 
            for j in range(i + 1, count): 
                new_x_i, new_x_j, S = self.NeedlemanWunsch(self.sequences[i], self.sequences[j]) 
                self.alignments[(i, j)] = (new_x_i, new_x_j, S)
                self.alignments[(j, i)] = (new_x_j, new_x_i, S)
                N_gaps = new_x_i.count('-') + new_x_j.count('-') 
                L = len(new_x_i) 
                S_rand = 0 
                for a in set(self.sequences[i]): 
                    for b in set(self.sequences[j]): 
                        S_ab = self.simMatrix[self.alphEnum[a], self.alphEnum[b]]  
                        N_i_a = self.sequences[i].count(a) 
                        N_i_b = self.sequences[j].count(b) 
                        S_rand += S_ab * N_i_a * N_i_b            
                S_rand /= L 
                S_rand += N_gaps * self.gapPenalty
                S_max = (S_ii[i] + S_ii[j]) / 2 
                D[str(i) + " " + str(j)] = -np.log(abs((S - S_rand) / (S_max - S_rand))) 
        self.distanceDictionary = D


    def buildTree(self):
        self.compute_D()
        upgma = Upgma(self.distanceDictionary, len(self.sequences))
        upgma.compute_clustering()
        self.newickTree = upgma.get_newick_tree()

    
    def computeOrderOfSequencesToAlign(self):
        indexBegin = 0
        indexEnd = len(self.newickTree)
        while indexEnd != -1:
            indexBegin = self.newickTree.rfind("(", indexBegin, indexEnd)
            if indexBegin == -1:
                break
            i = indexBegin + 1
            stack = 0
            while stack >= 0 and i < len(self.newickTree):
                if self.newickTree[i] == "(":
                    stack += 1
                elif self.newickTree[i] == ")":
                    stack -= 1
                i += 1
            indexEnd = i

            group0 = ""
            group1 = ""
            substring = self.newickTree[indexBegin:indexEnd]
            if substring[1] != "(":
                indexGroup0 = substring.find(",")
                group0 = substring[0:indexGroup0].strip(",")
                group1 = substring[indexGroup0:-1].strip(",")
            else:
                k = 1
                stack = 0
                while k < len(substring):
                    if substring[k] == "(":
                        stack += 1
                    elif substring[k] == ")":
                        stack -= 1
                    k += 1
                    if stack <= 0:
                        break
                group0 = substring[0:k].strip(",")
                group1 = substring[k:-1].strip(",")
            group0List = group0.split(",")
            group1List = group1.split(",")
            list0 = []
            list1 = []
            for j in group0List:
                list0.append(int(j.strip("(").strip(")").strip(",")))
            for j in group1List:
                list1.append(int(j.strip("(").strip(")").strip(",")))

            self.orderToAlign.append(sorted([sorted(list0), sorted(list1)]))
            indexEnd = indexBegin
            indexBegin = 0


    def computeMultipleAlignment(self):
        self.buildTree()
        self.computeOrderOfSequencesToAlign()
        result = self.sequences.copy()
        for align in self.orderToAlign:
            if len(align[0]) == 1 and len(align[1]) == 1:
                alignment = self.alignments[(align[0][0], align[1][0])]
                result[align[0][0]] = alignment[0]
                result[align[1][0]] = alignment[1]
            else:
                score = -float("inf")
                bestAlimgnment = None
                bestIndexes = None
                for i in align[0]:
                    for j in align[1]:
                        if self.alignments[(i, j)][-1] < score:
                            continue
                        score = self.alignments[(i, j)][-1]
                        bestAlimgnment = (self.alignments[(i, j)])
                        bestIndexes = (i, j)
                add_gaps =[[], []]
                for ind in range(2):
                    i = 1
                    while result[bestIndexes[ind]][-i] == "-":
                        add_gaps[1-ind].append(len(result[bestIndexes[ind]]) - i)
                        i += 1
                for ind in range(2):
                    j = 0
                    for i in range(len(bestAlimgnment[ind])):
                        while j < len(result[bestIndexes[ind]]) and result[bestIndexes[ind]][j] == "-":
                            add_gaps[1-ind].append(j)
                            j += 1
                        

                        if bestAlimgnment[ind][i] != "-":
                            j += 1
                            continue
                        for k in range(len(align[ind])):
                            result[align[ind][k]] = result[align[ind][k]][:j] + "-" + result[align[ind][k]][j:]
                        
                        j+=1
                for ind in range(2):
                    for gap in sorted(set(add_gaps[ind])):
                        for i in range(len(align[ind])):
                            result[align[ind][i]] = result[align[ind][i]][:gap] + "-" + result[align[ind][i]][gap:] 
                                    
        return result            
