import pytest
from FengDoolittle import FengDoolittle
import numpy as np

seqs = ["EGHIL", "QEGHI", "ILKM", "STWYVAR"]
with open("pam250.txt", 'r') as f:
        alphabet = f.readline() 
        while(alphabet[-1] in ['\n', '\r']): 
            alphabet = alphabet[:-1] 
        l = len(alphabet) 
        
        f.readline()
        d = int(f.readline()[:-1])
        f.readline() 
        simMatrix = np.ndarray((l, l)) 
        line = f.readline() 
        for i in range(l): 
            row = np.array(line.split()).astype(np.int32) 
            simMatrix[i] = row
            line = f.readline() 
    
alphEnum = dict([(alphabet[i], i) for i in range(len(alphabet))])  
    

def test_fengdoolitle():
    fd = FengDoolittle(seqs, simMatrix, -8, alphEnum)
    res = ["-EG-HIL-", "QEG-HI--", "-I--LKM-", "-STWYVAR"]
    assert res == fd.computeMultipleAlignment()

def test_upgma():
    fd = FengDoolittle(seqs, simMatrix, -8, alphEnum)
    fd.buildTree()
    tree = '(2,(3,(0,1)))'
    
    assert tree == fd.newickTree