import numpy
import matplotlib.pyplot as plt
from math import exp
from sys import argv
from sys import exit


def solution(t):
    x = -0.5 * exp(3 * t) + 1.5 * exp(-t)
    y = 0.5 * exp(3 * t) + 0.5 * exp(-t)
    z = 1.5 * exp(3 * t) - 0.5 * exp(-t)
    return x, y, z


if len(argv) > 1:
    file = open(argv[1], "r")
else:
    try:
        file = open("result.txt", "r")
    except FileNotFoundError:
        print("Такого файла не существует")
        exit()
lines = file.readlines()
file.close()
size_of_y = int(lines[0][:-1])
count_of_eps = int(lines[1][:-1])
epsilons = numpy.empty(count_of_eps)
min_h = numpy.empty(count_of_eps)
count_of_steps = numpy.empty(count_of_eps)
steps = []
ts = []
start = 2

for i in range(count_of_eps):
    epsilons[i] = float(lines[start][:-1])

    count_of_t = int(lines[start + 1][:-1])
    y = numpy.empty((count_of_t, size_of_y))
    t = numpy.empty(count_of_t)
    h = numpy.empty(count_of_t-1)

    for j in range(count_of_t):
        y[j] = numpy.fromstring(lines[start + 2 + j][:-1], count=3, sep=" ")
        if start + count_of_t + 2 + j == len(lines) -1:
            t[j] = lines[start + count_of_t + 2 + j]
        else:
            t[j] = lines[start + count_of_t + 2 + j][:-1]
        if j != 0:
            h[j-1] = t[j] - t[j-1]
    min_h[i] = numpy.min(h)
    count_of_steps[i] = len(h)

    f = plt.figure(1)
    plt.subplot(3, 1, 1)
    plt.plot(t, y.T[0], label="eps = {}".format(epsilons[i]))

    plt.subplot(3, 1, 2)
    plt.plot(t, y.T[1], label="eps = {}".format(epsilons[i]))

    plt.subplot(3, 1, 3)
    plt.plot(t, y.T[2], label="eps = {}".format(epsilons[i]))

    g = plt.figure(2)
    plt.plot(t[:-1], h, label="eps = {}".format(epsilons[i]))

    start += count_of_t * 2 + 2

my_solution = numpy.vectorize(solution)(t)

my_solution = numpy.array(my_solution)
f = plt.figure(1)
plt.subplot(3, 1, 1)
plt.plot(t, my_solution[0], label="solution")
plt.grid()
plt.title("решение X")
plt.xlabel("t")
plt.ylabel("x")
plt.legend(loc="best")

plt.subplot(3, 1, 2)
plt.plot(t, my_solution[1], label="solution")
plt.grid()
plt.title("решение Y")
plt.xlabel("t")
plt.ylabel("y")
plt.legend(loc="best")

plt.subplot(3, 1, 3)
plt.plot(t, my_solution[2], label="solution")
plt.grid()
plt.title("решение Z")
plt.xlabel("t")
plt.ylabel("z")
plt.legend(loc="best")

plt.savefig("solution.png")


f = plt.figure(2)
plt.grid()
plt.title("Изменение шага по отрезку")
plt.xlabel("t")
plt.ylabel("step")
plt.legend(loc="best")
plt.savefig("step_t.png")
plt.show()

f = plt.figure(3)
plt.semilogx(epsilons, min_h)
plt.grid()
plt.title("зависимость минимального шага от заданной точности")
plt.xlabel("eps")
plt.ylabel("min_h")
plt.savefig("min_step_eps.png")

f = plt.figure(4)
plt.semilogx(epsilons, count_of_steps)
plt.grid()
plt.title("зависимость числа шагов от заданной точности")
plt.xlabel("eps")
plt.ylabel("count_of_steps")
plt.savefig("count_of_steps_eps.png")



