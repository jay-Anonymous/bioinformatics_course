import numpy


def count_next(f, h, y, t):
    k1 = f(t, y) * h
    k2 = f(t + h/2, y + k1/2) * h
    k3 = f(t + h, y - k1 + 2 * k2) * h
    delta_y = (k1 + 4 * k2 + k3) / 6
    return y + delta_y


def runge_kutta_3(f, eps, y0, a, b):
    t = []
    y = []
    yk = y0
    tk = a
    h = 0.1
    while tk < b:
        y1 = count_next(f, h, yk, tk)
        y2 = count_next(f, h/2, yk, tk)
        y2 = count_next(f, h/2, y2, tk + h/2)
        delta = numpy.linalg.norm(y1 - y2) / (2 ** 3 - 1)
        if delta < eps:
            #print(h, tk)
            y.append(yk)
            t.append(tk)
            yk = y2
            tk += h
            if delta < eps / 64:
                h *= 2
                #print("*2", eps, tk, h)
        else:
            if h / 2 != h:
                h /= 2
        if b - tk < h:
            print("end", eps, tk, h)
            h = b - tk
            y.append(count_next(f, h, yk, tk))
            t.append(b)
            print(y[-1])
            print(t[-1])
            break

    return y, t




