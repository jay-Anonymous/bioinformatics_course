import numpy
from sys import argv
from method import runge_kutta_3


A = numpy.array([[-2, 1, -2],
                 [1, -2, 2],
                 [3, -3, 5]])

y0 = numpy.array([1., 1., 1.])
a = 0
b = 1


def f(t, y):
    return numpy.dot(A, y)


epsilons = []
if len(argv) > 2:
    for i in range(2, len(argv)):
        epsilons.append(float(argv[i]))
else:
    epsilons = [10 ** -i for i in range(1, 10, 3)]
if len(argv) > 1:
    filename = argv[1]
else:
    filename = "result.txt"
file = open(filename, "w")
file.write(str(len(A)) + "\n" + str(len(epsilons)))
for eps in epsilons:
    y, t = runge_kutta_3(f, eps, y0, a, b)

    file.write("\n" + str(eps))
    file.write("\n" + str(len(t)))
    file.write("\n" + "\n".join(" ".join(map(str, x)) for x in y))
    file.write("\n" + "\n".join(map(str, t)))

file.close()

