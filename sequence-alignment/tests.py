import pytest
from HMM import *

def test_backward():
    x = "TTACG" 
    eps = 10 ** (-5)
    y = "TAG" 
    model = HMM("HMM.json")
    bM, bX, bY = model.backward(x, y)
    assert 7.574 * 10 ** (-5) - eps < bM[0, 0] < 7.574 * 10 ** (-5) + eps
    assert 5.653 * 10 ** (-5) - eps < bX[0, 0] < 5.653 * 10 ** (-5) + eps
    assert 2.716 * 10 ** (-5) - eps < bY[0, 0] < 2.716 * 10 ** (-5) + eps
    # значения взяты из книжки "Задания и решения по анализу биологических последовательностей с. 164"
    # если значения [0, 0] совпадают -> все остальные тоже верны, тк использвуются последовательно


def test_seqs_len():
    model = HMM("HMM.json")
    try:
        model.backward("", "GCAT")
    except ZeroLenException:
        assert 1
    try:
        model.backward("CAT", "")
    except ZeroLenException:
        assert 1
    try:
        model.backward("", "")
    except ZeroLenException:
        assert 1

def test_empty_json():
    try:
        model = HMM("empty.json")
    except JsonFileException:
        assert 1


def test_json_without_field():
    try:
        model = HMM("without_eps.json")
    except JsonFileException:
        assert 1


def test_file_doesnt_exists():
    try:
        model = HMM("blabla.json")
    except JsonFileException:
        assert 1
