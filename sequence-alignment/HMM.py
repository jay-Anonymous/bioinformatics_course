import numpy as np
import json
import os
class JsonFileException(Exception):
    def __init__(self, message) -> None:
        self.message = message
        super().__init__(message)


class ZeroLenException(Exception):
    def __init__(self) -> None:
        self.message = "sequences must have non-zero length"
        super().__init__("sequences must have non-zero length")

class HMM: 
    def __init__(self, json_filename): 
        if not os.path.exists(json_filename):
            raise JsonFileException(f"file '{json_filename}' doesn't exists")
        with open(json_filename, 'r') as file:
            try:
                data = json.load(file)
            except:
                raise JsonFileException("json file error")
        if "P" not in data:
            raise JsonFileException("json file must contains 'P'")
        if "q" not in data:
            raise JsonFileException("json file must contains 'q'")
        if "eps" not in data:
            raise JsonFileException("json file must contains 'eps'")
        if "tau" not in data: 
            raise JsonFileException("json file must contains 'tau'")
        if "delta" not in data: 
            raise JsonFileException("json file must contains 'delta'")
        self.P = data["P"]   
        self.q = data["q"]
        self.eps = data["eps"] 
        self.tau = data["tau"]
        self.delta = data["delta"] 
         
    
    def backward(self, x, y): 

        len_x = len(x) 
        len_y = len(y)
        if len_x == 0 or len_y == 0:
            raise ZeroLenException 
        bM = np.full((len_x, len_y), self.tau) 
        bX = np.full((len_x, len_y), self.tau) 
        bY = np.full((len_x, len_y), self.tau)
         
 
        for j in range(len_y - 1, -1, -1): 
            for i in range(len_x - 1, -1, -1): 
                if i == len_x - 1 and j == len_y - 1: 
                    continue 
             
                cur_bM = 0 if i == len_x - 1 or j == len_y - 1 else self.P[x[i+1] + y[j+1]] * bM[i+1][j+1] 
                
                cur_bX = 0 if i == len_x - 1 else self.q[x[i+1]] * bX[i+1][j] 
 
                cur_bY = 0 if j == len_y - 1 else self.q[y[j+1]] * bY[i][j+1] 
 
                bM[i][j] = (1 - 2 * self.delta - self.tau) * cur_bM + self.delta * (cur_bX + cur_bY) 
                bX[i][j] = (1 - self.eps - self.tau) * cur_bM + self.eps * cur_bX 
                bY[i][j] = (1 - self.eps - self.tau) * cur_bM + self.eps * cur_bY 
                print(bM[i][j], bX[i][j], bY[i][j])
         
        print("bM:") 
        print(bM) 
        print("\nbX:") 
        print(bX) 
        print("\nbY:") 
        print(bY) 
        return bM, bX, bY
