
from sys import argv

if len(argv) < 3:
    print("You need to use: python main.py <sequences_filename.fasta> <HMM_filename.json>")
    exit()
from Bio import SeqIO
from HMM import HMM


seqs = []
for s in SeqIO.parse(argv[1], "fasta"):
    seqs.append(s.seq)
if len(seqs) != 2: 
    print("Fasta file must contains two sequences")
    exit()

if all([let.isalpha() == False for let in seqs[0]]) or all([let.isalpha() == False for let in seqs[1]]):
    print("Sequences must only letters")
    exit()

model = HMM(argv[2])
if model == None:
    exit()
model.backward(*seqs)




 
